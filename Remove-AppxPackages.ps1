Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.MicrosoftSolitaireCollection | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.MicrosoftOfficeHub | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.YourPhone -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.YourPhone | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.ZuneMusic -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.ZuneMusic | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.ZuneVideo -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.ZuneVideo | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.MixedReality.Portal -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.MixedReality.Portal | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.SkypeApp -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.SkypeApp | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.BingWeather -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.BingWeather | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.XboxApp -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.XboxApp | Remove-AppxProvisionedPackage -Online

Get-AppxPackage -Name Microsoft.XboxGamingOverlay -AllUsers | Remove-AppxPackage
Get-AppXProvisionedPackage -Online | where DisplayName -EQ Microsoft.XboxGamingOverlay | Remove-AppxProvisionedPackage -Online
